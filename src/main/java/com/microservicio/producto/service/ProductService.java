/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microservicio.producto.service;

import com.microservicio.producto.model.Product;
import java.util.List;

/**
 *
 * @author infan
 */
public interface ProductService {
    
  public List<Product> findAllProduct();
  public Product findProductById(Long id);
    
}
