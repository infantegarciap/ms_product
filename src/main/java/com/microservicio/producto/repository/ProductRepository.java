/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microservicio.producto.repository;

import com.microservicio.producto.model.Product;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author infan
 */
public interface ProductRepository extends CrudRepository<Product, Long>{
  
}
